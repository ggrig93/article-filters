<?php


namespace App\Contracts;


interface ProductsInterface
{
    /**
     * @return mixed
     */
    public function index();

    /**
     * @return mixed
     */
    public function fetchProducts();
}
