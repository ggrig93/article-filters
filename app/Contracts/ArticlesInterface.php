<?php


namespace App\Contracts;

use App\Models\Article;
use Illuminate\Http\Request;

interface ArticlesInterface
{
    /**
     * @param Article $article
     */
    public function update(Article $article);

    /**
     * @param Article $article
     */
    public function show(Article $article);

    /**
     * @param Request $request
     */
    public function filter(Request $request);

    /**
     *
     */
    public function fetchArticles();
}
