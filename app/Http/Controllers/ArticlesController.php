<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Product;
use App\Repositories\ArticlesRepository;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{

    protected $articlesRepository;

    public function __construct(ArticlesRepository $articlesRepository)
    {
        $this->articlesRepository = $articlesRepository;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\JsonResponse
     */
    public function fetchArticles()
    {
        return $this->articlesRepository->fetchArticles();
    }

    /**
     *  Filter data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request)
    {
        return $this->articlesRepository->filter($request);
    }

    /**
     *  Show article
     *
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Article $article)
    {
        return $this->articlesRepository->show($article);
    }

    /**
     *  Update article
     *
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Article $article)
    {
        return $this->articlesRepository->update($article);
    }

}
