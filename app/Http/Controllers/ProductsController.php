<?php

namespace App\Http\Controllers;

use App\Repositories\ProductsRepository;

class ProductsController extends Controller
{

    protected $productsRepository;

    public function __construct(ProductsRepository $productsRepository)
    {
        $this->productsRepository = $productsRepository;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->productsRepository->index();
    }

    public function fetchProducts()
    {
        return $this->productsRepository->fetchProducts();
    }

}
