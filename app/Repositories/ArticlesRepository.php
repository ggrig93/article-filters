<?php


namespace App\Repositories;


use App\Contracts\ArticlesInterface;
use App\Models\Article;
use App\Models\Product;
use Illuminate\Http\Request;

class ArticlesRepository implements ArticlesInterface
{
    public function fetchArticles()
    {
        return Article::with('product')->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request)
    {
        $filterBy = $request->filter_by;
        $articles = Article::query();

        if(is_int($filterBy)) {
            $articles->where('product_id', $filterBy);
        } else {
            $articles->orderBy($filterBy, 'DESC')->get();
        }

        $articles = $articles->with('product')->get();

        return response()->json([
            'articles' => $articles
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Article $article)
    {
        $product = Product::where('id', $article->product_id)->first();

        return response()->json([
            'article' => $article,
            'product' => $product,
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Article $article)
    {
        $product = Product::where('id', $article->product_id)->first();

        $article->views += 1;
        $article->save();

        return response()->json([
            'article' => $article,
            'product' => $product,
        ]);
    }
}
