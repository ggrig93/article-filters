<?php


namespace App\Repositories;


use App\Contracts\ProductsInterface;
use App\Models\Product;

class ProductsRepository implements ProductsInterface
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        return view('landing');
    }

    /**
     * @return Product[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function fetchProducts()
    {
        return Product::all();
    }
}
