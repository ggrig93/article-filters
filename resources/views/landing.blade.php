@extends('layouts.app')
@section('content')
    <div>
        <router-view filter-route="{{ route('filter') }}" fetch-products="{{ route('fetch.products')}}" fetch-articles="{{ route('fetch.articles')}}"></router-view>
    </div>
@endsection
