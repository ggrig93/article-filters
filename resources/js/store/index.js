import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        products: [],
        articles: [],
        article: '',
        product: '',
    },
    mutations: {
        FETCH_PRODUCTS(state, products) {
            return state.products = products
        },
        FETCH_ARTICLES(state, articles) {
            return state.articles = articles
        },
        FETCH_ARTICLE(state, article) {
            return state.article = article
        },
        FETCH_PRODUCT(state, product) {
            return state.product = product
        },
    },
    getters: {
        products: state => {
            return state.products
        },
        articles: state => {
            return state.articles
        },
        article: state => {
            return state.article
        },
        product: state => {
            return state.product
        },
    },
    actions: {
        async filter({commit}, data) {

            let filterBy = {
                'filter_by': data.filterBy
            };

            await axios.post(data.url, filterBy)
                .then(res => {
                    commit('FETCH_ARTICLES', res.data.articles)
                }).catch(err => {
                    console.log(err)
                })
        },
        async fetchProducts({commit}, url) {
            await axios.get(url)
                .then(res => {
                    commit('FETCH_PRODUCTS', res.data);
                }).catch(err => {
                    console.log(err)
                })
        },
        async fetchArticles({commit}, url) {
            await axios.get(url)
                .then(res => {
                    commit('FETCH_ARTICLES', res.data);
                }).catch(err => {
                    console.log(err)
                })
        },
        async fetchArticle({commit}, data) {
            await axios.get('/show-article/'+data.articleId)
                .then(res => {
                    commit('FETCH_ARTICLE', res.data.article);
                    commit('FETCH_PRODUCT', res.data.product);
                }).catch(err => {
                    console.log(err)
                })
        },
        async updateArticle({commit}, data) {

            await axios.put('/update-article/'+data.article_id)
                .then(res => {
                    commit('FETCH_ARTICLE', res.data.article);
                    commit('FETCH_PRODUCT', res.data.product);
                }).catch(err => {
                    console.log(err)
                })
        }
    }
})
