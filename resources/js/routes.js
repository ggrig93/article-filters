import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Blog from "./components/Blog";
import Article from "./components/Article";

const routes = [
    {
        path: '/',
        component: Blog
    },
    {
        path: '/article/:id',
        component: Article
    }
]

export default new VueRouter({
    mode: "history",
    routes
})
