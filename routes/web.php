<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\ProductsController::class, 'index'])->name('landing');
Route::get('/article/{article}', [\App\Http\Controllers\ProductsController::class, 'index'])->name('landing');
Route::get('/fetch-products', [\App\Http\Controllers\ProductsController::class, 'fetchProducts'])->name('fetch.products');
Route::get('/fetch-articles', [\App\Http\Controllers\ArticlesController::class, 'fetchArticles'])->name('fetch.articles');
Route::get('/show-article/{article}', [\App\Http\Controllers\ArticlesController::class, 'show'])->name('show.article');
Route::put('/update-article/{article}', [\App\Http\Controllers\ArticlesController::class, 'update'])->name('update.article');
Route::post('/filter', [\App\Http\Controllers\ArticlesController::class, 'filter'])->name('filter');
