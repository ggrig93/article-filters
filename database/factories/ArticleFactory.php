<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $subDays = Carbon::now()->subDays(mt_rand(1, 100));

        return [
            'title' => $this->faker->title,
            'short_description' => $this->faker->sentence(5),
            'body' => $this->faker->sentence(55),
            'views' => 0,
            'created_at' => $subDays,
        ];
    }
}
