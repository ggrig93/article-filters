<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Product::count() && Article::count()) {
            return false;
        }

        Product::factory(5)
            ->has(Article::factory()->count(15), 'articles')
            ->create();
    }
}
